% 1.

% ------------------------ Base
% e ∊ T_{>,<,+,-,.,[,]}(D)

% --------- Empty case
% e ∊ Prog

% IcInst, PcProg
% --------------- Decomposition
% I P ∊ Prog

% ---------
% > ∊ Inst

% ---------
% < ∊ Inst

% ---------
% + ∊ Inst

% ---------
% - ∊ Inst

% P ∊ Prog
% ---------
% [P] ∊ Inst

% 2.

% /!\ [ et ] ne sont pas des atomes, s'ils sont à l'interieur d'une Expr, elle doit être ecrite 'Expr'
instr(>).
instr(<).
instr(+).
instr(-).
instr(.).
instr(CProgC) :- atom_concat('[', ProgC, CProgC), atom_concat(Prog, ']', ProgC), prog(Prog).

prog().
prog(Instr) :- instr(Instr).
prog(ProgInstr) :- atom_concat(Prog, Instr, ProgInstr), instr(Instr), prog(Prog).
