% ------------------ Syntaxe ------------------- %

prog([]).
prog([Inst|Prog]) :- inst(Inst), prog(Prog).

inst(+).
inst(-).
inst(<).
inst(>).
inst([Inst|Prog]) :- inst(Inst), prog(Prog).

% ------------------ Sémantique ------------------- %

% Fonction modify qui ajoute la valeur N à un élément d'une liste
modify([X|Y], 0, N, [Res|Y]) :- Res is X + N.
modify([X|Y], Pt, N, [X|TabRes]) :-
  Pt \= 0,
  Pt1 is Pt-1,
  modify(Y, Pt1, N, TabRes).

get([X|_], 0, X).
get([_|Y], Pt, Res) :- Pt1 is Pt-1, get(Y, Pt1, Res).

% Decomposition d'un programme
evalP(Tab, Pt, [], Tab, Pt).
evalP(Tab, Pt, [I|P], Tab2, Pt2) :-
  evalI(Tab, Pt, I, Tab1, Pt1),
  evalP(Tab1, Pt1, P, Tab2, Pt2).

% + et -
evalI(Tab, Pt, +, Tab1, Pt) :- modify(Tab, Pt, 1, Tab1).
evalI(Tab, Pt, -, Tab1, Pt) :- modify(Tab, Pt, -1, Tab1).

% < et >
evalI(Tab, Pt, <, Tab, Pt1) :- Pt1 is Pt-1.
evalI(Tab, Pt, >, Tab, Pt1) :- Pt1 is Pt+1.

% Cas boucle 1
evalI(Tab, Pt, P, Tab1, Pt1) :-
  evalP(Tab, Pt, P, Tab1, Pt1),
  get(Tab1, Pt1, 0).

% Cas boucle 2
evalI(Tab, Pt, P, Tab2, Pt2) :-
  evalP(Tab, Pt, P, Tab1, Pt1),
  evalI(Tab1, Pt1, P, Tab2, Pt2),
  get(Tab1, Pt1, R),
  R \= 0.

% Exemple d'utilisation:

% Cell-clear: evalP([10], 0, [[-]], Tab,Pt).
% Add: evalP([3,4], 0, [[-,>,+,<]], Tab,Pt).
% Seek: evalP([2,3,6,1,4], 0, [-,[+,>,-],+], Tab,Pt).
