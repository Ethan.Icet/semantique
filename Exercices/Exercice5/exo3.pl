% 1. Domaine Semantique
% Tableau de valeurs: Tab
% Pointeur de la position courante: Pt
% Programme: Prog
% => Tab × Pt × Prog

% 2. Opérateurs
% Evaluation programme =>_p : Tab × Pt × Prog -> Tab × Pt
% Evaluation instruction =>_I : Tab × Pt × Prog -> Tab × Pt

% modify : Tab × Pt × Int -> Tab

% ------------------------------- Empty
% {Tab, Pt, e} =>_p {Tab, Pt}

% {Tab, Pt, I} =>_I {Tab',Pt'}, {Tab', Pt', P} =>_P {Tab'',Pt''} 
% -------------------------------------------------------------- Prog
% {Tab, Pt, I P} =>_p {Tab'', Pt''}

% modify(Tab, Pt, 1} = Tab'
% --------------------------- +
% {Tab, Pt, +} =>_I {Tab',Pt}

% modify(Tab, Pt, -1} = Tab'
% --------------------------- -
% {Tab, Pt, -} =>_I {Tab',Pt}

% Pt' = Pt+1
% ---------------------------- >
% {Tab, Pt, >} =>_I {Tab, Pt'}

% Pt' = Pt-1
% ---------------------------- <
% {Tab, Pt, <} =>_I {Tab, Pt'}

% {Tab, Pt, P} =>_P {Tab', Pt'}, Tab'[Pt'] = 0
% ---------------------------------------------- Do
% {Tab, Pt, [P]} =>_I {Tab', Pt'}

% {Tab, Pt, P} =>_P {Tab', Pt'}, {Tab', Pt', [P]} =>_I {Tab'', Pt''},  Tab'[Pt'] ≠ 0
% ----------------------------------------------------------------------------------- While
% {Tab, Pt, [P]} =>_I {Tab'', Pt''}


% 3. Prolog

% ++++++++++++++ Syntaxe ++++++++++++++
% /!\ [ et ] ne sont pas des atomes, s'ils sont à l'interieur d'une Expr, elle doit être ecrite 'Expr'
instr(>).
instr(<).
instr(+).
instr(-).
instr(.).
instr(CProgC) :- atom_concat('[', ProgC, CProgC), atom_concat(Prog, ']', ProgC), prog(Prog).

prog().
prog(Instr) :- instr(Instr).
prog(ProgInstr) :- atom_concat(Prog, Instr, ProgInstr), instr(Instr), prog(Prog).

% ++++++++++++++ Semantique ++++++++++++++

get(_, Pt, _) :- Pt < 0, fail.
get([X|_], 0, X).
get([_|Tab], Pt, TabPt) :- Pt1 is Pt - 1, get(Tab, Pt1, TabPt).

% modify(Tab, Pt, Incr, Tab2, Pt2)

% LowerBound and UpperBound 
in_bound(Pt) :- 0 < Pt, Pt < 100.

modify([X|Tab], 0, Incr, [XIncr|Tab]) :- XIncr is X + Incr.
modify([X|Tab], Pt, Incr, [X|Tab1]) :- in_bound(Pt), Pt1 is Pt - 1, modify(Tab, Pt1, Incr, Tab1).


evalP(Tab, Pt, '', Tab, Pt) :- is_list(Tab), in_bound(Pt).
evalP(Tab, Pt, InstrProg, Tab2, Pt2) :- is_list(Tab), in_bound(Pt), atom_concat(Instr, Prog, InstrProg), evalI(Tab, Pt, Instr, Tab1, Pt1), evalP(Tab1, Pt1, Prog, Tab2, Pt2).

evalI(Tab, Pt, +, Tab1, Pt) :- modify(Tab, Pt, 1, Tab1).

evalI(Tab, Pt, -, Tab1, Pt) :- modify(Tab, Pt, -1, Tab1).

evalI(Tab, Pt, >, Tab, Pt1) :- in_bound(Pt), Pt1 is Pt + 1, in_bound(Pt1).

evalI(Tab, Pt, <, Tab, Pt1) :- in_bound(Pt), Pt1 is Pt - 1, in_bound(Pt1).


