% 1.
% op5([], 0).
% op5([H|T], s(X)) :- op5(T, X).
% length

% 2.
% op6([], _, []).
% op6([H|T], X, [NewH|Res]) :- NewH is H + X, op6(T, X, Res).
% ajoute X a tout les elements d'une liste

% 3.
% op7([X], X).
% op7([H|T], X) :- op7(T,X), X > H.
% op7([H|T], H) :- op7(T,X), X =< H.
% max

