% Les vaches sont séparées par de l'herbe et la dernière suivie d'un arbre.
% 1. la liste pourrait faire partie du troupeau car on a pas de restriction sur t
% (il n'est pas écrit que les vaches ne sont séparées que par 1 plant d'herbe, ou que l'arbre est forcément précédé d'une vache).

vache(a). vache(b). vache(c). vache(d). vache(e). vache(f). vache(g). vache(h). vache(i). vache(j).
vache(k). vache(l). vache(m). vache(n). vache(o). vache(p). vache(q). vache(r). vache(s). vache(t).
vache(u). vache(v). vache(w). vache(x). vache(y). vache(z).

troupeau([tree]).
troupeau([V,herbe|T]) :- vache(V), troupeau(T).

% 4.

% ------------
% nb(tree) = 0

% VcVache, TcTroupeau, nb(T) = N
% ------------------------------
% nb(V herbe T) = N+1

nb([tree], 0).
nb([V, herbe|T], Res) :- vache(V), nb(T, N), Res is N + 1;


% 5

% Initialisation :

% -------------
% nb(tree) = 0
% -------------
% nb(tree) >= 0


% Hérédité :
% ----------  ------
% nb(T) >= 0, 1 >= 0
% ------------
% nb(T)+1 >= 0
% ------------------
% nb(V herbe T) >= 0


