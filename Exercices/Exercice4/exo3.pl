% 1.

% -------------
% palindrome(e)

% -------------
% Xc{A,B}, palindrome(X)

% Xc{A,B}, palindrome(P)
% ----------------------
% palindrome(X|P|X)

alphabet(a).
alphabet(b).

palindrome([]).
palindrome([X]) :- alphabet(X).

palindrome([X|PX]) :- alphabet(X), append(P, [X], PX), palindrome(P).


% 2.

% ------------
% nb_A(e) = 0

% ------------
% nb_A(A) = 1

% ------------
% nb_A(B) = 0

% PcPalindrome, nb_A(P) = N
% -------------------------
% nb_A(A|P|A) = N + 2

% PcPalindrome, nb_A(P) = N
% -------------------------
% nb_A(B|P|B) = N


nb_a([], 0).
nb_a([a], 1).
nb_a([b], 0).
nb_a([a|PA], NB_A) :- append(P, [a], PA), nb_a(P, N), NB_A is N + 2.
nb_a([b|PB], NB_A) :- append(P, [b], PB), nb_a(P, NB_A).

nb_b([], 0).
nb_b([b], 1).
nb_b([a], 0).
nb_b([b|PB], NB_B) :- append(P, [b], PB), nb_b(P, N), NB_B is N + 2.
nb_a([b|PB], NB_B) :- append(P, [a], PA), nb_a(P, NB_B).



% 3.

% Initialisation:

% -----------  -----------
% nb_A(e) = 0, nb_B(e) = 0

% -----------  -----------
% nb_A(A) = 1, nb_B(A) = 0

% -----------  -----------
% nb_A(B) = 0, nb_B(B) = 1


% Heredite:
% Soit Pair : Int -> Bool

% ---------------
% Pair(0)

% XcInt Pair(X), Y = X+2
% ---------------
% Pair(Y)


% Soit Parité : Int -> {Pair, Impair}

% ----------------
% Parité(0) = Pair

% ----------------
% Parité(1) = Impair

% XcInt, Pc{Pair, Impair}, Parité(X) = P, Y = X+2
% -------------------------------------
% Parité(Y) = P


% X,YcInt, Parite(X) = Parite(Y), Pair(X)
% ------------------------------
% Pair(Y)


% 		----------------------------- hyp Rec
% PcPalindrome, Pair(nb_a(P)) ou Pair(nb_b(P), XcAlphabet, X|P|XcPalindrome
%		----------------------------------------------------------------------------
% 		nb_a(X|P|X) = nb_a ou nb_a(X|P|X) = nb_a + 2, nb_b(X|P|X) = nb_b ou nb_b(X|P|X) = nb_b + 2
%		----------------------------------------------------------------------------
% 		Parité(nb_a(P)) = Parité(nb_a(X|P|X)), Parité(nb_b(P)) = Parité(nb_b(X|P|X))
% 		----------------------------------------------------------------------------
%		Pair(nb_a(X|P|X)) ou Pair(nb_b(X|P|X)

%	      ------------------------ ---------------------  -----------------------  ---------------------
% XcAlphabet, nb_A(a|P|a) = nb_A(P)+2, nb_A(b|P|b) = nb_A(P), nb_B(b|P|b) = nb_B(P)+2, nb_A(a|P|a) = nb_A(P) 
% ----------  ----------------------------------------------  ----------------------------------------------  hyp_rec-------------------------
% 	      Parite(nb_A(x|P|x)) = Parite(nb_A(P)),          Parite(nb_B(x|P|x)) = Parite(nb_B(P)),          Parite(nb_A(P))=/=Parite(nb_B(P))
% 	      ---------------------------------------------------------------------------------------------------------------------------------
% 	      Parite(nb_A(x|P|x))=/=Parite(nb_B(x|P|x)







