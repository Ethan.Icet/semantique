% 2.3

vache(a).
vache(b).
vache(c).
vache(d).
vache(e).
vache(f).
troupeau(arbre).
troupeau(X) :- vache(X).
% ...
% To complete if you want all the possibilities

langageVache([tree]).
langageVache([H1|[herbe|T]]) :- troupeau(H1), langageVache(T).

% 2.4
nb([tree],0).
nb([H1|[herbe|T]], Res) :- troupeau(H1), nb(T,R), Res is R+1.
