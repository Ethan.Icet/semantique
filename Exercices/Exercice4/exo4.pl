% 1. Arbre binaire

% --------------
% feuille c Arbre

% XcArbre, YcArbre
% ----------------
% noeud(X, Y) c Arbre

arbre(feuille).
arbre(noeud(X,Y)) :- arbre(X), arbre(Y).


% 2. Nombre de Noeuds
% nb_n : Arbre -> Int
% Mq nb_n(A) = nb_n(A1) + nb_n(A2) + 1, pour A = node(A1,A2) 

% Cas initial
% -----------------
% nb_n(feuille) = 1

% Cas Recursif
% Ajout droite
% -------------------------------- Construit de manière a ne rajouter qu'un noeud...
% nb_n(node(A,feuille)) = nb_n(A) + 1
% ----------------------------------------------
% nb_n(node(A,feuille)) = nb_n(A) + nb_n(feuille) + 1

% Ajout Gauche
% Similaire

% Pour rajouter un élément à un arbre on doit nécéssairement changer un noeud avec la regle de l'ajout droite/gauche.
% ce qui par somme succéssive va augmenter la taille de l'arbre de + 1.

nb_n(feuille, 1).
nb_n(noeud(G,D), N) :- nb_n(G, Ng), nb_n(D, Nd), N is (Ng+Nd+1).


% 3. Hauteur 

% hauteur : Arbre -> Int

% Mq AcArbre, hauteur(A) = max(hauteur(A1),hauteur(A2)), A = node(A1,A2)

% Cas Initial
% -----------------
% hauteur(feuille) = 1

% Cas Recursif
% Ajout Droite
% ----------------------<Definition
% AcArbre Hauteur(A) >=1
% -------------------------
% Hauteur(node(A,feuille)) = Hauteur(A) + 1
% --------------------------------------
% Hauteur(node(A,feuille)) = max(Hauteur(A), Hauteur(feuille)) + 1

% similaire pour cas gauche


hauteur(feuille, 1).
hauteur(noeud(G,D), H) :- hauteur(G, Hg), hauteur(D, Hd), H is max(Hg, Hd) + 1.

