% Factoriel 

% factoriel(X, R) :- !, X < 0.
factoriel(0, 1).
factoriel(X, Res) :- Y is X-1, factoriel(Y, A), Res is A*X.
