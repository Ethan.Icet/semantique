% Somme

% 1)
enum(X, X, [X]).
% enum(P, D, Res) :- A is P+1, enum(A, D, R), append([P],R, Res).
enum(P, D, [P|R]) :- A is P+1, enum(A, D, R).
% enum(P, D, Res) :- A is P+1, enum(A, D, R), Res = [P|R].
% XXX enum(P, D, Res) :- A is P+1, enum(A, D, R), Res is [P|R].

% 2)
somme([], 0).
somme([P|L], Res) :- somme(L, R), Res is R+P.
