% Palindrome

% 1)
renverser([], []).
renverser([A|L1], L) :- renverser(L1, L2), append(L2, [A], L).

% 2)
palindrome(Mot) :- renverser(Mot,Mot).
% palindrome([]).
% palindrome(X) :- append(L1, L2, X), renverser(L1, L2).
% palindrome(X) :- append(L1, [_|L2], X), renverser(L1, L2).
