% HW 4 - Ex 4.5

%% HELPER - Atom -> list conversion to have more appealing call
% split atom separated by space into list
atom_to_list('',[]) :- !.
atom_to_list(A,[A]) :- atom_space(A), !.
atom_to_list(A_Z,[A|L]) :- atom_concat(A_,Z,A_Z), atom_concat(A,' ', A_),
    atom_space(A), atom_length(A, LengthA), LengthA > 0, !,
    atom_length(Z, LengthZ), LengthZ > 0, atom_to_list(Z,L).

% True if no spaces in atom
atom_space('') :- !.
atom_space(' ') :- !, fail.
atom_space(X) :- atom_length(X, 1), !.
atom_space(XZ) :- atom_concat(X, Z, XZ), atom_length(X, 1), !,
    X \= ' ', atom_space(Z).

nomValide('')		:- !, fail.
nomValide(':')		:- !, fail.
nomValide(';')		:- !, fail.
nomValide('+') 	:- !, fail.
nomValide('-') 	:- !, fail.
nomValide('*') 	:- !, fail.
nomValide('/') 	:- !, fail.
nomValide('SWAP')	:- !, fail.
nomValide('DUP')	:- !, fail.
nomValide('OVER')	:- !, fail.
nomValide('DROP')	:- !, fail.
nomValide('IF')	:- !, fail.
nomValide('THEN')	:- !, fail.
nomValide('ELSE')	:- !, fail.
nomValide(Na) 		:- atom_space(Na), atom_number(Na, N), integer(N), !, fail.
nomValide(NV) 		:- atom_space(NV).

opL('=').
opL('<').
opL('>').

%% Semantique :
% forth(Prog, Fct, Stack, FctOut, StackOut)
forth('', F, S, F, S).
forth(IP, F, S, F2, S2) :- atom_concat(I_, P, IP), atom_concat(I,' ', I_), I \='', P \= '',
	forth(I, F, S, F1, S1), forth(P, F1, S1, F2, S2).

forth(Na, F, S, F, [N|S]) :- atom_space(Na), atom_number(Na, N), integer(N).

forth('+', F, [X0,X1|S], F, [X|S]) :- X is X0 + X1.
forth('-', F, [X0,X1|S], F, [X|S]) :- X is X0 - X1.
forth('*', F, [X0,X1|S], F, [X|S]) :- X is X0 * X1.
forth('/', F, [X0,X1|S], F, [X|S]) :- X is X0 / X1.

forth('SWAP', F, [X0,X1|S], F, [X1,X0|S]).
forth('DUP' , F, [X0|S],    F, [X0,X0|S]).
forth('OVER', F, [X0,X1|S], F, [X1,X0,X1|S]).
forth('DROP', F, [_|S],     F, S).

forth(Def, F, S, [[Nom, Corps]|F], S) :- atom_concat(': ', NCSc, Def), atom_concat(NC, Sc_, NCSc),
	atom_concat(Nom_, Corps, NC), atom_concat(Nom,' ',Nom_), nomValide(Nom),
	(Corps = '' -> Sc_ = ';' ; Sc_ = ' ;'),
	aggregate_all(count, member([Nom,_],F), 0).

forth(Call, F, S, F1, S1) :- aggregate_all(count, member([Call,Corps],F), 1), member([Call,Corps],F), forth(Corps, F, S, F1, S1).

forth('=', F, [X,X|S], F, [1|S]).
forth('=', F, [X,Y|S], F, [0|S]) :- X \= Y.
forth('<', F, [X,Y|S], F, [1|S]) :- X < Y.
forth('<', F, [X,Y|S], F, [0|S]) :- X >= Y.
forth('>', F, [X,Y|S], F, [1|S]) :- X > Y.
forth('>', F, [X,Y|S], F, [0|S]) :- X >= Y.

% On fait le then else en une opération comme on le peux facilement
forth(Cond, F, S, F1, S1) :- atom_concat(OpL, IPeEPtT, Cond), opL(OpL),
	atom_concat(' IF ', PeEPtT, IPeEPtT), atom_concat(Pe, EPtT, PeEPtT),
	atom_concat(ELSE_, PtT, EPtT), atom_concat(Pt, THEN_, PtT),
	(Pe = '' -> ELSE_ = 'ELSE ' ; ELSE_ = ' ELSE '),(Pt = '' -> THEN_ = 'THEN' ; THEN_ = ' THEN'),
	forth(OpL, F, S, F, [TF|S0]),
	(TF = 0 -> forth(Pe, F, S0, F1, S1) ; (TF = 1, forth(Pt, F, S0, F1, S1))).

test(Cond, S, Out) :- atom_concat(OpL, IPeEPtT, Cond), opL(OpL),
	atom_concat(' IF ', PeEPtT, IPeEPtT), atom_concat(Pe, EPtT, PeEPtT),
	atom_concat(ELSE_, PtT, EPtT), atom_concat(Pt, THEN_, PtT),
	(Pe = '' -> ELSE_ = 'ELSE ' ; ELSE_ = ' ELSE '),(Pt = '' -> THEN_ = 'THEN' ; THEN_ = ' THEN'),
	forth(OpL, [], S, [], [TF|S0]),
	(TF = 0 -> Out = Pe ; (TF = 1, Out = Pt)).
