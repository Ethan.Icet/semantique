% HW 4 - Ex 3.5

%% HELPER - Atom -> list conversion to have more appealing call
% split atom separated by space into list
atom_to_list('',[]) :- !.
atom_to_list(A,[A]) :- atom_space(A), !.
atom_to_list(A_Z,[A|L]) :- atom_concat(A_,Z,A_Z), atom_concat(A,' ', A_),
    atom_space(A), atom_length(A, LengthA), LengthA > 0, !,
    atom_length(Z, LengthZ), LengthZ > 0, atom_to_list(Z,L).

% True if no spaces in atom
atom_space('') :- !.
atom_space(' ') :- !, fail.
atom_space(X) :- atom_length(X, 1), !.
atom_space(XZ) :- atom_concat(X, Z, XZ), atom_length(X, 1), !,
    X \= ' ', atom_space(Z).

nomValide('')		:- !, fail.
nomValide(':')		:- !, fail.
nomValide(';')		:- !, fail.
nomValide('+') 	:- !, fail.
nomValide('-') 	:- !, fail.
nomValide('*') 	:- !, fail.
nomValide('/') 	:- !, fail.
nomValide('SWAP')	:- !, fail.
nomValide('DUP')	:- !, fail.
nomValide('OVER')	:- !, fail.
nomValide('DROP')	:- !, fail.
nomValide(Na) 		:- atom_space(Na), atom_number(Na, N), integer(N), !, fail.
nomValide(NV) 		:- atom_space(NV).


%% Semantique :
% forth(Prog, Fct, Stack, FctOut, StackOut)
forth('', F, S, F, S).
forth(IP, F, S, F2, S2) :- atom_concat(I_, P, IP), atom_concat(I,' ', I_), I \='', P \= '',
	forth(I, F, S, F1, S1), forth(P, F1, S1, F2, S2).

forth(Na, F, S, F, [N|S]) :- atom_space(Na), atom_number(Na, N), integer(N).

forth('+', F, [X0,X1|S], F, [X|S]) :- X is X0 + X1.
forth('-', F, [X0,X1|S], F, [X|S]) :- X is X0 - X1.
forth('*', F, [X0,X1|S], F, [X|S]) :- X is X0 * X1.
forth('/', F, [X0,X1|S], F, [X|S]) :- X is X0 / X1.

forth('SWAP', F, [X0,X1|S], F, [X1,X0|S]).
forth('DUP' , F, [X0|S],    F, [X0,X0|S]).
forth('OVER', F, [X0,X1|S], F, [X1,X0,X1|S]).
forth('DROP', F, [_|S],     F, S).

forth(Def, F, S, [[Nom, Corps]|F], S) :- atom_concat(': ', NCSc, Def), atom_concat(NC, Sc_, NCSc),
	atom_concat(Nom_, Corps, NC), atom_concat(Nom,' ',Nom_), nomValide(Nom),
	(Corps = '' -> Sc_ = ';' ; Sc_ = ' ;'),
	aggregate_all(count, member([Nom,_],F), 0).

forth(Call, F, S, F1, S1) :- aggregate_all(count, member([Call,Corps],F), 1), member([Call,Corps],F), forth(Corps, F, S, F1, S1).
