% HW 4 - Ex 1.3

% On considère les éléments comme des entiers

% Opérandes minimales
stack(empty, []).
stack(push(P,E), [E|P]) :- is_list(P), integer(E).
stack(top([H|_]), H).
stack(pop([_|T]), T).

% Opérandes Instructions
stack(push(P_Ins,E), Eval) :- stack(P_Ins, P), stack(push(P,E), Eval).
stack(push(P,E_Ins), Eval) :- stack(E_Ins, E), stack(push(P,E), Eval).
stack(top(P_Ins), H) :- stack(P_Ins, P), stack(top(P), H).
stack(pop(P_Ins), T) :- stack(P_Ins, P), stack(pop(P), T).

