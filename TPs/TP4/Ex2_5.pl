% HW 4 - Ex 2.5

%% HELPER - Atom -> list conversion to have more appealing call
% split atom separated by space into list
atom_to_list('',[]) :- !.
atom_to_list(A,[A]) :- atom_space(A), !.
atom_to_list(A_Z,[A|L]) :- atom_concat(A_,Z,A_Z), atom_concat(A,' ', A_),
    atom_space(A), atom_length(A, LengthA), LengthA > 0, !,
    atom_length(Z, LengthZ), LengthZ > 0, atom_to_list(Z,L).

% True if no spaces in atom
atom_space('') :- !.
atom_space(' ') :- !, fail.
atom_space(X) :- atom_length(X, 1), !.
atom_space(XZ) :- atom_concat(X, Z, XZ), atom_length(X, 1), !,
    X \= ' ', atom_space(Z).


%% Semantique :
% forth(Prog, Pc, LabelPtr, Stack, PcOut, LabelPtrOut, StackOut)
forth('', Pc, L, S, Pc, L, S).
forth(Prog, Pc, L, S, Pc, L, S) :- atom_to_list(Prog, P),
    length(P, Length), Length =< Pc, !.
forth(Prog, Pc, L, S, Pc2, L2, S2) :- atom_to_list(Prog, P), nth0(Pc,P,I), !,
	evalI(I, Pc, L, S, Pc1, L1, S1), forth(Prog, Pc1, L1, S1, Pc2, L2, S2).

evalI(Na, Pc, L, S, Pc1, L, [N|S]) :- atom_number(Na, N), integer(N),
    Pc1 is Pc+1.

evalI('+', Pc, L, [X0,X1|S], Pc1, L, [X|S]) :- X is X0 + X1, Pc1 is Pc+1.
evalI('-', Pc, L, [X0,X1|S], Pc1, L, [X|S]) :- X is X0 - X1, Pc1 is Pc+1.
evalI('*', Pc, L, [X0,X1|S], Pc1, L, [X|S]) :- X is X0 * X1, Pc1 is Pc+1.
evalI('/', Pc, L, [X0,X1|S], Pc1, L, [X|S]) :- X is X0 / X1, Pc1 is Pc+1.

% On effectue l'assignation si aucune n'a été faite avec le label
% sinon on vérifie qu'il n'existe que celle-ci  
evalI(LABEL, Pc, L, S, Pc1, L1, S) :- atom_concat('LABEL=', N, LABEL),
    ((not(member([N,_], L)), L1 = [[N,Pc]|L]) ;
    (aggregate_all(count, member([N,PtrN],L), 1), member([N,PtrN],L), L1 = L)),
	Pc1 is Pc+1.

evalI(JUMP, _, L, [0|S], Pc1, L, [0|S]) :- atom_concat('JUMP=', N, JUMP),
    aggregate_all(count, member([N,PtrN], L), 1), member([N,PtrN], L),
    Pc1 is PtrN+1.
evalI(JUMP, Pc, L, [X0|S], Pc1, L, [X0|S]) :- atom_concat('JUMP=', N, JUMP),
    aggregate_all(count, member([N,_], L), 1), X0 \= 0,
    Pc1 is Pc+1.

evalI('SWAP', Pc, L, [X0,X1|S], Pc1, L, [X1,X0|S]) :-    Pc1 is Pc+1.
evalI('DUP', Pc, L, [X0|S], Pc1, L, [X0,X0|S]) :-        Pc1 is Pc+1.
evalI('OVER', Pc, L, [X0,X1|S], Pc1, L, [X1,X0,X1|S]) :- Pc1 is Pc+1.
evalI('DROP', Pc, L, [_|S], Pc1, L, S) :-                Pc1 is Pc+1.
