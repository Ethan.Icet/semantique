%%% Exercice 1

% 1. porte(De, Vers)

%   A B C D
% 1 S
% 2     M
% 3     S
% 4 E     E

porte(b1, a1).
porte(c1, b1).
porte(d1, c1).

porte(a2, a1).
porte(b2, a2).
porte(b2, c2).
porte(c2, c1).
porte(c2, d2).
porte(c2, c3).
porte(d2, d1).
porte(d2, d3).

porte(a3, b3).
porte(b3, b2).

porte(a4, a3).
porte(b4, b3).
porte(c4, b4).
porte(c4, c3).
porte(d4, c4).

% 2.

entree(a4).
entree(d4).

sortie(a1).
sortie(c3).

minotaure(c2).


%%% Exercice 2:

% 1. chemin(De, Vers)
chemin(De, Vers) :- porte(De, Vers).
chemin(De, Vers) :- porte(De, X), chemin(X, Vers).

% 2. itineraire(De, Vers, Pieces)
itineraire(De, Vers, [De, Vers]) :- porte(De, Vers).
itineraire(De, Vers, [De, X | P]) :- porte(De, X), itineraire(X, Vers, [X|P]).

%itineraire(De, Vers, [De|P]) :- porte(De, X), itineraire(X, Vers, P), append([X], _, P).

%%% Exercise 3:

% 1. batterie(Pieces, Batterie, Reste)
batterie([_|[]], X, X) :- X >= 0.
batterie([_|P], Batterie, Reste) :- batterie(P, Batterie, R), Reste is R-1, Reste >= 0.

test_batterie(De, Vers, Batterie, Reste) :- itineraire(De, Vers, Pieces), batterie(Pieces, Batterie, Reste).

% Ancien Test
% batterie([_|P], Batterie, Reste) :- length(Pieces,X), Reste is Batterie+1-X, Reste >= 0.

% batterie([_|[]], X, X).
% batterie([_|P], Batterie, Reste) :- batterie(P, Batterie, R), Reste is R-1, Reste >= 0.
% batterie([_|P], Batterie, Reste) :- batterie(P, B, Reste), Batterie is B+1, Reste >= 0.
% batterie(Pieces, Batterie, Reste) :- batterie(Pieces, B, R), Batterie is B + 1, Reste is R + 1.


% 2. chemin_batterie(De, Vers, Batterie, Pieces, Reste)
chemin_batterie(De, Vers, Batterie, Pieces, Reste) :- itineraire(De, Vers, Pieces), batterie(Pieces, Batterie, Reste).

% 3. chemin_reussite(Batterie, Pieces)
chemin_reussite(Batterie, Pieces) :- minotaure(Minotaure), entree(Entree), sortie(Sortie),
				     chemin_batterie(Entree, Minotaure, Batterie, PM, BM),
				     chemin_batterie(Minotaure, Sortie, BM, [Minotaure|PS], _),
				     append(PM, PS, Pieces).


%%% Exercise 4:

% 1. reussite_complete(Batterie, Pieces)
reussite_complete(Batterie, Pieces) :- BR is Batterie - 7, chemin_reussite(BR, Pieces).

% 2. reussite_tweet(Batterie, Pieces)

% Idees
% On pourrait prendre les combinaisons qui arrivent à tweeter et enlever reusite_complete
% On pourrait verifier que l'on fini sur une non-sortie, soit un cul de sac, soit car on a plus de batterie
% Ces méthodes marchent dans l'hypothèse que les sorties sont des noeuds terminaux, sinon on aurait des resultats où on est passer par une sortie mais on a continué ailleurs. 


% Helper
cul_de_sac(X) :- ((sortie(X);porte(X, _)) -> fail ; true).

% On fini sans batterie sur le minotaure
% On fini (avec batterie) sur un cul de sac
% On fini sans batterie sur une case non sortie
reussite_tweet(Batterie, Pieces) :- minotaure(Minotaure), entree(Entree), BT is Batterie - 7, 
				    chemin_batterie(Entree, Minotaure, BT, Pieces, 0).

reussite_tweet(Batterie, Pieces) :- minotaure(Minotaure), entree(Entree), BT is Batterie - 7,
				    chemin_batterie(Entree, Minotaure, BT, PM, RM),
				    chemin_batterie(Minotaure, Fin, RM, [Minotaure|PS], Reste),
				    Reste > 0, cul_de_sac(Fin), append(PM, PS, Pieces).

reussite_tweet(Batterie, Pieces) :- minotaure(Minotaure), entree(Entree), BT is Batterie - 7,
				    chemin_batterie(Entree, Minotaure, BT, PM, RM),
				    chemin_batterie(Minotaure, Fin, RM, [Minotaure|PS], 0),
				    (sortie(Fin) -> fail ; append(PM, PS, Pieces)).


% Ancien Test
%reussite_tweet(Batterie, Pieces) :- append(_ , [X|[]], Pieces), sortie(X), fail.
%reussite_tweet(Batterie, Pieces) :- chemin_batterie(entree(_), minotaure(_), Batterie, Pieces, 0).
%reussite_tweet(Batterie, Pieces) :- append(P, [X|[]], Pieces), reussite_tweet(B, P), B is Batterie - 1, B >= 0, X 

%reussite_tweet(Batterie, Pieces) :- minotaure(Minotaure), entree(Entree), sortie(Sortie), BT is Batterie - 7, chemin_batterie(Entree, Minotaure, BT, PM, BM), (chemin_batterie(Minotaure, Sortie, BM, _, _) -> fail ; chemin_batterie(Minotaure, piece(_), BM, [Minotaure|PS], _), append(PM, PS, Pieces) ; Pieces = PM).

                         
