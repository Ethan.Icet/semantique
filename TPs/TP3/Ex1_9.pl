% HW 3 - Ex 1.9

% Cas vide
tradHO("","").

% Cas de base (1 element)
tradHO("AtGa", "Groa Gru" ).
tradHO("AtDr", "Gru Groa" ).
tradHO("AtFr", "Gru Gru"  ).
tradHO("DeGa", "Grou Groa").
tradHO("DeDr", "Groa Grou").
tradHO("DeFr", "Grou Grou").
tradHO("M"   , "Gru Grou" ).
tradHO("S"   , "Grou Gru" ).

% Cas Recursif (>= 2 elements)
tradHO(E_H,EPO) :- string_concat(E_, H, E_H), string_concat(ExprH, " ", E_),
		   		   tradHO(ExprH,ExprO),
		   		   tradHO(H,O), H\="", O\="",
		   		   string_concat(ExprO, " P ",EP), string_concat(EP, O, EPO).
