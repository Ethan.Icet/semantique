% HW 3 - Ex 1.2

expr_H("AtGa").
expr_H("AtDr").
expr_H("AtFr").

expr_H("DeGa").
expr_H("DeDr").
expr_H("DeFr").

expr_H("M").
expr_H("S").


% Cas de base (0 element)
phrase_H("").
% Cas Recursif (>= 1 element)
phrase_H(ES) :- expr_H(E), string_concat(E,S_,ES),
				((S_ = "");
				(string_concat(" ",S,S_), S \= "", phrase_H(S))).
