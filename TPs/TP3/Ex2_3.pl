% HW 3 - Ex 2.3

expr_B("Bru Bra Bro").
expr_B("Bro Bra Bru").
expr_B("Bro Bro Bro").
expr_B("Bra Bru Bro").
expr_B("Bro Bru Bra").
expr_B("Bra Bra Bra").
expr_B("Bru Bru Bra").
expr_B("Bra Bru Bru").

% Cas vide
phrase_B("").
% Cas de base (1 element)
phrase_B(Expr) :- expr_B(Expr).
% Cas Recursif (>= 2 elements)
phrase_B(EPS)  :- expr_B(E), string_concat(E," P ",EP),
				  string_concat(EP,S,EPS),					
				  S \= "", phrase_B(S).
