% HW 3 - Ex 1.5

expr_O(Expr) :-  G1 = "Groa",
				(G2 = "Gru" ; G2 = "Grou"),
				string_concat(G1," ",G1_), string_concat(G1_,G2,Expr).
expr_O(Expr) :- (G2 = "Gru" ; G2 = "Grou"),
				(G3 = "Gru" ; G3 = "Grou" ; G3 = "Groa"),
				string_concat(G2," ",G2_), string_concat(G2_,G3,Expr).
% Cas vide
phrase_O("").
% Cas de base (1 element)
phrase_O(Expr) :- expr_O(Expr).
% Cas Recursif (>= 2 elements)
phrase_O(EPS)  :- expr_O(E), string_concat(E," P ",EP),
				  string_concat(EP,S,EPS),					
				  S \= "", phrase_O(S).		% Evite un P final 
