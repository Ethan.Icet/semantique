% HW 3 - Ex 2.7

tradOB("","").
tradOB("Groa Gru" , "Bru Bra Bro").
tradOB("Gru Groa" , "Bro Bra Bru").
tradOB("Gru Gru"  , "Bro Bro Bro").
tradOB("Grou Groa", "Bra Bru Bro").
tradOB("Groa Grou", "Bro Bru Bra").
tradOB("Grou Grou", "Bra Bra Bra").
tradOB("Gru Grou" , "Bru Bru Bra").
tradOB("Grou Gru" , "Bra Bru Bru").

tradOB(EPO,EPB) :- string_concat(EPo, O, EPO), string_concat(ExprO, " P ",EPo),
		   		   tradOB(ExprO,ExprB),
		   		   tradOB(O,B), O\="", B\="",
		   		   string_concat(ExprB, " P ",EPb), string_concat(EPb, B, EPB).
